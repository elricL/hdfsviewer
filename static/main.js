var app = angular.module( 'myApp', ['ui.bootstrap'] );
app.controller('HbaseController', function($scope, $http, $q) {
	$scope.reset = function () {
		$q.all()
 		scan = $http.get('/scan');
 		fsck = $http.get('/fsck');

 		$q.all([scan, fsck]).then(function(results) {
 			$scope.scan = results[0].data;
 			$scope.scan_values = new Array;
			$scope.server_list = [];
			$scope.region_list = []
			for(var o in $scope.scan) {
				$scope.server_list.push($scope.scan[o].server);
			    $scope.scan_values.push($scope.scan[o]);
			}
 			$scope.fsck = results[1].data;

 			$scope.region_files = {}

 			for (fname_str in $scope.fsck){
 				fname = fname_str.split("/");
 				if(fname.length < 6){
 					continue;
 				}
 				region_name = fname[5];
 				if ($scope.region_files[region_name] === undefined ){
 					$scope.region_files[region_name] = [];
 					$scope.region_list.push(region_name);
 				}
 				$scope.region_files[region_name].push($scope.fsck[fname_str]);
 			}
  		})
	}
	$scope.reset()
});