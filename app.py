import subprocess
import fixtures.fsck_fixture
import helpers.json_helper
from flask import Flask, render_template
app = Flask(__name__)


@app.route("/scan")
def scan():
        pipe1 = subprocess.Popen(["echo",'scan "hbase:meta"'], stdout=subprocess.PIPE)
        pipe2 = subprocess.Popen(["/usr/lib/hbase/bin/hbase", "shell"],  stdin=pipe1.stdout, stdout=subprocess.PIPE)
	pipe1.stdout.close()
	val = pipe2.communicate()[0]
        print val
	return helpers.json_helper.get_json_meta_scan(val)

@app.route("/fsck")
def fsck():
	val = subprocess.Popen("hdfs fsck /hbase -racks -files -blocks".split(),stdout=subprocess.PIPE).communicate()[0]
	return helpers.json_helper.get_json_fsck(val)


@app.route("/")
def index():
	return render_template("view.html")

if __name__ == "__main__":
	app.run(host='0.0.0.0',debug=True)
