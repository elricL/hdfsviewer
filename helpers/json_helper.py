import simplejson as json
import re

def get_json_fsck(fsck):
	output = {}
	it = iter(fsck.split("\n"))
	for line in it:
		if line.startswith("/") and (not (line.endswith("<dir>"))):
			mini_dict = dict(zip( ("fname", "fsize", "fsize_unit", "block_count") ,tuple(line[:-14].split())))
			fname = mini_dict["fname"]
			block_list = []
			for i in range(int(mini_dict["block_count"])):
				block_line = next(it)
				index, bid, blen, repl, rpl_list = tuple(block_line.split(" ",4))
				blen = blen.split("=")[1]
				repl = repl.split("=")[1]
				rpl_list = sorted(rpl_list.replace("/default-rack/","").replace("[","").replace("]","").replace(",","").strip().split())
				block_dict = {"id" : bid,"index" : index, "blen" : blen, "repl": repl, "replist" : rpl_list}
				block_list.append(block_dict)
			output[fname] = {"file_data" :mini_dict, "block_data" : block_list}
	return json.dumps(output)


def get_json_meta_scan(hbase_scan):
	output = {}
	it = iter(hbase_scan.split("\n")[:-3])
	# skip first two lines
	next(it)
	next(it)
	next(it)
	next(it)
	next(it)
	next(it)
	region_data = {}
	for line in it:
		line = line.strip()
		if not line:
			continue
		key, column = line.split(" ",1)
		if key not in region_data:
			region_data[key] = {}
                print key,column 
                print "DDDDDDDDDDDDDDDDDDD"
		line_dict = dict([ tuple(map(str.strip,col.split("=", 1))) for col in column.split(",",2)])
		if line_dict["column"] == "info:seqnumDuringOpen":
			region_data[key]["seq_num"] = line_dict["value"]
		elif line_dict["column"] == "info:server":
			region_data[key]["server"] = line_dict["value"]
		elif line_dict["column"] == "info:serverstartcode":
			region_data[key]["server_startcode"] = line_dict["value"]
		elif line_dict["column"] == "info:regioninfo":
			value = line_dict["value"]
			search = re.search(r"{ENCODED => (.*), NAME => '(.*)', STARTKEY => '(.*)', ENDKEY => '(.*)'}",value)
			if search:
				region_data[key]["encoded"] = search.group(1)
				region_data[key]["name"] = search.group(2)
				region_data[key]["start_key"] = search.group(3)
				region_data[key]["end_key"] = search.group(4)
	return json.dumps(region_data)
